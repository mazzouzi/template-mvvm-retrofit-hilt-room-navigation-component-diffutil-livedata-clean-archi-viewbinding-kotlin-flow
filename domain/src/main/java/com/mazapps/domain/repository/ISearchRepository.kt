package com.mazapps.domain.repository

import com.mazapps.domain.model.Search
import com.mazapps.domain.state.Resource
import kotlinx.coroutines.flow.Flow

/**
 * @author morad.azzouzi on 11/11/2020.
 */
interface ISearchRepository {

    suspend fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Flow<Resource<List<Search>?>>
}