package com.mazapps.domain.state

import com.mazapps.domain.state.StatusEnum.*

/**
 * @author morad.azzouzi on 13/12/2020.
 */
data class Resource<out T>(
    val status: StatusEnum,
    val data: T?,
    val message: String?,
    val code: Int?
) {

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null, null)
        }

        fun <T> error(data: T? = null, msg: String? = null, code: Int? = null): Resource<T> {
            return Resource(ERROR, data, msg, code)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(LOADING, data, null, null)
        }
    }
}

enum class StatusEnum {
    LOADING,
    SUCCESS,
    ERROR
}

