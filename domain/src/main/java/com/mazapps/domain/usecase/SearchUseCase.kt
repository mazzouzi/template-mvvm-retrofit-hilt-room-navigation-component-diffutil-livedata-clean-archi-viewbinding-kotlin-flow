package com.mazapps.domain.usecase

import com.mazapps.domain.model.Search
import com.mazapps.domain.repository.ISearchRepository
import com.mazapps.domain.state.Resource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * @author morad.azzouzi on 23/12/2020.
 */
class SearchUseCase @Inject constructor(private val searchRepository: ISearchRepository) {

    suspend operator fun invoke(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Flow<Resource<List<Search>?>> = searchRepository.fetchSearchInfo(
        action,
        format,
        list,
        keyword
    )
}