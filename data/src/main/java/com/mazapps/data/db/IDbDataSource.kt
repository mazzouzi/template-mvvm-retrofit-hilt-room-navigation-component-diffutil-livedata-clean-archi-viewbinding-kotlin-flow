package com.mazapps.data.db

import com.mazapps.data.model.SearchData
import com.mazapps.domain.state.Resource

/**
 * @author morad.azzouzi on 13/11/2020.
 */
interface IDbDataSource {

    fun fetchRelatedSearch(): Resource<List<SearchData>>

    fun insert(search: SearchData)
}