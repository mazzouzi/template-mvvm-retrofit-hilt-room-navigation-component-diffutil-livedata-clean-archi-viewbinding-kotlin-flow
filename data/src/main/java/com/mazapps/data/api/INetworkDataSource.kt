package com.mazapps.data.api

import com.mazapps.data.model.SearchData
import com.mazapps.domain.state.Resource
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow

/**
 * @author morad.azzouzi on 01/12/2019.
 */
interface INetworkDataSource {

    suspend fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Resource<List<SearchData>>
}