package com.mazapps.data.repository

import com.mazapps.data.api.INetworkDataSource
import com.mazapps.data.db.IDbDataSource
import com.mazapps.data.mapper.toDomain
import com.mazapps.domain.model.Search
import com.mazapps.domain.repository.ISearchRepository
import com.mazapps.domain.state.Resource
import com.mazapps.domain.state.StatusEnum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * @author morad.azzouzi on 01/12/2019.
 */
class SearchRepository @Inject constructor(
    private val networkDataSource: INetworkDataSource,
    private val dbDataSource: IDbDataSource
) : ISearchRepository {

    override suspend fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Flow<Resource<List<Search>?>> = flow {
        try {
            val localData = dbDataSource.fetchRelatedSearch()
            if (localData.status == StatusEnum.SUCCESS) {
                emit(Resource.success(localData.data?.toDomain()))
            }
        } catch (e: Exception) {
            // do nothing and wait for network response
        }

        val remoteData = networkDataSource.fetchSearchInfo(action, format, list, keyword)
        if (remoteData.status == StatusEnum.SUCCESS) {
            emit(Resource.success(remoteData.data?.toDomain()))
            remoteData.data?.forEach { dbDataSource.insert(it) }
        } else {
            emit(Resource.error(null, code = remoteData.code))
        }
    }
        .catch {
            try {
                val localData = dbDataSource.fetchRelatedSearch()
                if (localData.status == StatusEnum.SUCCESS) {
                    emit(Resource.success(localData.data?.toDomain()))
                } else {
                    emit(Resource.error(null))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emit(Resource.error(null))
            }
        }
        .flowOn(Dispatchers.IO)
}