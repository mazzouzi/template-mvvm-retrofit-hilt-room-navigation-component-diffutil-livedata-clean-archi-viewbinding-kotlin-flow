package com.mazapps.template.helper

import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewbinding.ViewBinding
import com.mazapps.template.ui.interfaces.ViewBindingInterface

class ViewBindingHelper<T : ViewBinding> : ViewBindingInterface<T>, LifecycleObserver {

    override var binding: T? = null
    var lifecycle: Lifecycle? = null

    private lateinit var name: String

    override fun initBinding(
        binding: T,
        lifecycleOwner: LifecycleOwner,
        viewName: String,
        onBound: (T.() -> Unit)?
    ): View {
        this.binding = binding
        lifecycle = lifecycleOwner.lifecycle
        lifecycle?.addObserver(this)
        name = viewName
        onBound?.invoke(binding)
        return binding.root
    }

    override fun requireBinding(block: (T.() -> Unit)?) =
        binding?.apply {
            block?.invoke(this)
        } ?: throw IllegalStateException("Accessing binding outside of Fragment lifecycle: $name")

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroyView() {
        lifecycle?.removeObserver(this)
        lifecycle = null
        binding = null
    }
}
