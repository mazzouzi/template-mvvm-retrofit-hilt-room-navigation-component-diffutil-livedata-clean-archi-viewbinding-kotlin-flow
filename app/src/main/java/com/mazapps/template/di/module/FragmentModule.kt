package com.mazapps.template.di.module

import android.content.Context
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.ui.searchinfo.view.SearchInfoAdapter
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.FragmentScoped
import io.reactivex.disposables.CompositeDisposable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Module
@InstallIn(FragmentComponent::class)
class FragmentModule {

    @FragmentScoped
    @Provides
    fun provideItemDecorator(
        @ActivityContext context: Context
    ) = DividerItemDecoration(context, RecyclerView.VERTICAL)

    @FragmentScoped
    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @ExperimentalStdlibApi
    @FragmentScoped
    @Provides
    fun providerSearchInfoAdapter(viewModel: SearchInfoViewModel): SearchInfoAdapter =
        SearchInfoAdapter(viewModel)
}