package com.mazapps.template.di.module

import com.mazapps.domain.repository.ISearchRepository
import com.mazapps.domain.usecase.SearchUseCase
import com.mazapps.template.ui.searchinfo.viewmodel.SearchInfoViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Module
@InstallIn(ActivityRetainedComponent::class)
class ActivityRetainedModule {

    @Provides
    fun provideUseCase(searchRepository: ISearchRepository) = SearchUseCase(searchRepository)

    @ActivityRetainedScoped
    @Provides
    fun provideSearchInfoViewModel(
        searchUseCase: SearchUseCase
    ): SearchInfoViewModel = SearchInfoViewModel(searchUseCase)
}