package com.mazapps.template.framework.mapper

import com.mazapps.data.model.SearchData
import com.mazapps.template.model.entity.SearchEntity

/**
 * @author morad.azzouzi on 25/12/2020.
 */
internal fun List<SearchEntity>.toData(): List<SearchData> =
    this.map {
        SearchData(
            it.ns,
            it.title,
            it.pageid,
            it.size,
            it.snippet,
            it.timestamp,
            it.wordcount
        )
    }
