package com.mazapps.template.framework.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mazapps.template.model.entity.SearchEntity

/**
 * @author morad.azzouzi on 13/11/2020.
 */
@Database(
    entities = [SearchEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun daoSearch(): DaoSearch

    companion object {

        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            if (instance == null) {
                synchronized(AppDatabase::class) {
                    instance = Room.databaseBuilder(
                        context,
                        AppDatabase::class.java,
                        "database.db"
                    ).build()
                }
            }
            return instance!!
        }
    }
}