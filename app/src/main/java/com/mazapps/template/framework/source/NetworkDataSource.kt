package com.mazapps.template.framework.source

import com.mazapps.data.api.INetworkDataSource
import com.mazapps.data.model.SearchData
import com.mazapps.domain.state.Resource
import com.mazapps.template.framework.api.RetrofitApiService
import com.mazapps.template.framework.mapper.toData
import javax.inject.Inject

/**
 * @author morad.azzouzi on 01/12/2019.
 */
open class NetworkDataSource @Inject constructor(
    private val retrofit: RetrofitApiService
) : INetworkDataSource {

    override suspend fun fetchSearchInfo(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Resource<List<SearchData>> {
        val result = retrofit.fetchSearchInfo(action, format, list, keyword)
        return if (result.body() != null && result.isSuccessful) {
            Resource.success(result.body().toData())
        } else {
            Resource.error(code = result.code())
        }
    }
}