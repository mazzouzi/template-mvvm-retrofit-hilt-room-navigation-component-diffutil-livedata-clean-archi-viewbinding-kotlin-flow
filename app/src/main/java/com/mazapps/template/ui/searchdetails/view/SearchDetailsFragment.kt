package com.mazapps.template.ui.searchdetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.mazapps.template.databinding.FragmentSearchDetailsBinding
import com.mazapps.template.helper.ViewBindingHelper
import com.mazapps.template.ui.interfaces.ViewBindingInterface
import com.mazapps.template.ui.searchdetails.viewmodel.SearchDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchDetailsFragment :
    Fragment(),
    ViewBindingInterface<FragmentSearchDetailsBinding> by ViewBindingHelper() {

    private val viewModel: SearchDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initBinding(
        FragmentSearchDetailsBinding.inflate(inflater),
        viewLifecycleOwner,
        SearchDetailsFragment::class.java.name
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        requireBinding {
            title.text = viewModel.searchData?.title
            wordCount.text = viewModel.searchData?.wordcount.toString()
            pageId.text = viewModel.searchData?.pageid.toString()
            size.text = viewModel.searchData?.size.toString()
        }
    }
}