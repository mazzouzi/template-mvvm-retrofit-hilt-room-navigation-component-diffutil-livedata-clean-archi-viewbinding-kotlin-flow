package com.mazapps.template.ui.searchinfo.viewmodel

import androidx.lifecycle.viewModelScope
import com.mazapps.domain.model.Search
import com.mazapps.domain.state.Resource
import com.mazapps.domain.state.StatusEnum
import com.mazapps.domain.usecase.SearchUseCase
import com.mazapps.template.ui.base.BaseViewModel
import com.mazapps.template.ui.searchinfo.mapper.toPresentation
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * @author morad.azzouzi on 11/11/2020.
 */
const val KEYWORD = "sasuke"
class SearchInfoViewModel(
    private val searchUseCase: SearchUseCase
) : BaseViewModel<MutableList<Pair<SearchInfoEnum, Any>>>() {

    val itemCount: Int
        get() = data?.size ?: 0
    private val isEmpty: Boolean
        get() = itemCount == 0

    fun getItemViewTypeAt(position: Int): Int = data?.get(position)?.first?.ordinal ?: 0
    fun getItemDataAt(position: Int): Any? = data?.get(position)?.second

    init {
        _items.value = Resource.loading()
        fetchSearchInfo()
    }

    private fun fetchSearchInfo() {
        viewModelScope.launch {
            searchUseCase("query", "json", "search", KEYWORD)
                .collect {
                    when (it.status) {
                        StatusEnum.LOADING -> _items.value = Resource.loading()
                        StatusEnum.SUCCESS -> onSuccess(it.data)
                        StatusEnum.ERROR -> onError(it.message)
                    }
                }
        }
    }

    private fun onSuccess(search: List<Search>?) {
        search?.let {
            mutableListOf<Pair<SearchInfoEnum, Any>>().apply {
                add(Pair(SearchInfoEnum.HEADER, KEYWORD))
                addAll(search.map { Pair(SearchInfoEnum.SEARCH, it.toPresentation()) })
                _items.value = Resource.success(this)
            }
        }
    }

    private fun onError(message: String?) {
        if (isEmpty) {
            _items.value = Resource.error(msg = message)
        }
    }
}

enum class SearchInfoEnum {
    HEADER,
    SEARCH
}