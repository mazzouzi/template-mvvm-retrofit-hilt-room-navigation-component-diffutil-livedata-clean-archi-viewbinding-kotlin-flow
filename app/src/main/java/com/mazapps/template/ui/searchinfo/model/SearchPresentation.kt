package com.mazapps.template.ui.searchinfo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @author morad.azzouzi on 25/12/2020.
 */
@Parcelize
data class SearchPresentation(
    val ns: Int,
    val title: String,
    val pageid: Int,
    val size: Int,
    val snippet: String,
    val timestamp: String,
    val wordcount: Int
) : Parcelable