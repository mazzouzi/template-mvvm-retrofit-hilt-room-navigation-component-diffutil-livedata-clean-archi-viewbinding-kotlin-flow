package com.mazapps.template.ui.searchinfo.event

data class SearchInfoClickEvent(val position: Int)