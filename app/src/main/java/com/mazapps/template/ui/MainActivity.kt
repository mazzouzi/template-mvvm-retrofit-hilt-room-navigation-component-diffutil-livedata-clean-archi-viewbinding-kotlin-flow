package com.mazapps.template.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.mazapps.template.R
import com.mazapps.template.databinding.ActivityMainBinding
import com.mazapps.template.helper.ViewBindingHelper
import com.mazapps.template.ui.interfaces.ViewBindingInterface
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity :
    AppCompatActivity(),
    ViewBindingInterface<ActivityMainBinding> by ViewBindingHelper() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding(
            ActivityMainBinding.inflate(layoutInflater),
            this,
            MainActivity::class.java.name
        )
        setContentView(binding?.root)
        initToolbar()
    }

    private fun initToolbar() {
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        requireBinding {
            toolbar.setupWithNavController(navController, appBarConfiguration)
        }
    }
}