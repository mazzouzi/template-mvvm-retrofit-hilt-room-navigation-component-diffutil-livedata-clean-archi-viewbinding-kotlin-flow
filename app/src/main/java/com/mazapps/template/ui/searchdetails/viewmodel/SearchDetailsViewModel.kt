package com.mazapps.template.ui.searchdetails.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.mazapps.template.ui.searchinfo.model.SearchPresentation

/**
 * @author morad.azzouzi on 13/12/2020.
 */
class SearchDetailsViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val searchData: SearchPresentation? = savedStateHandle.get<SearchPresentation>(SEARCH_DATA_KEY)

    companion object {
        const val SEARCH_DATA_KEY = "searchData"
    }
}