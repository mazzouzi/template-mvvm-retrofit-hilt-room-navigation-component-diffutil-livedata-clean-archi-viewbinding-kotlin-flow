package com.mazapps.template.ui.interfaces

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding

/**
 * Holds and manages ViewBinding inside Activity / Fragment.
 */
interface ViewBindingInterface<T : ViewBinding> {

    val binding: T?

    /**
     * Saves the binding for cleanup on onDestroy, calls the specified function [onBound] with
     * `this` value as its receiver and returns the bound view root.
     */
    fun initBinding(
        binding: T,
        lifecycleOwner: LifecycleOwner,
        viewName: String,
        onBound: (T.() -> Unit)? = null
    ): View

    /**
     * Calls the specified [block] with the binding as `this` value and returns the binding.
     * As a consequence, this method can be used with a code block lambda in [block] or
     * to initialize a variable with the return type.
     *
     * @throws IllegalStateException if not currently holding a ViewBinding
     * (when called outside of an active view's lifecycle)
     */
    fun requireBinding(block: (T.() -> Unit)? = null): T
}
