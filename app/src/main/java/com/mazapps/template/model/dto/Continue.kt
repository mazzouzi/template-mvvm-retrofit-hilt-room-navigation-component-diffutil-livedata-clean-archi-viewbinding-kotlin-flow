package com.mazapps.template.model.dto

data class Continue(
    val `continue`: String?,
    val sroffset: Int?
)